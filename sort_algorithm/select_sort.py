import numpy as np
import time

def swap(arr, index1, index2):
    tmp = arr[index1]
    arr[index1] = arr[index2]
    arr[index2] = tmp

def find_min_arr(arr):
    return arr.index(min(arr)), min(arr)

def select_sort(arr):
    for i in range(len(arr)):
        index, value = find_min_arr(arr[i:len(arr)])
        swap(arr, i, index+i)

size = 10000
my_arr = list(np.random.randint(0, 500, size=size)) # 10M
start = time.time()
select_sort(my_arr)
stop = time.time()
print("Select sort run {} elements in {}s".format(size, stop - start))
# print(my_arr)