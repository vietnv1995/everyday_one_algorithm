import time
import numpy as np

def merge(arr, l, m, r):
    tmp = []
    i = j = 0
    while (i < m-l+1) and (j < r-m+1):
            if arr[i] < arr[j]:
                tmp.append(arr[l+i])
                i += 1
            else:
                tmp.append(arr[r+j])
                j+=1

    while i < m-l+1:
        tmp.append(arr[i])
        i+=1
    while j < r-m+1:
        tmp.append(arr[j])
        j+=1
    k = 0
    for i in range(l, r+1):
        arr[i] = tmp[k]
        k+=1




def merge_sort(arr, l, r):
    m = (r + l) // 2
    if r-l > 1:
        merge_sort(arr, l, m)
        merge_sort(arr, m+1, r)
        merge(arr, l, m, r)
    # else:
    #     merge(arr, l, m, r)

arr = [12, 11, 13, 5, 6, 7]
merge_sort(arr, 0, len(arr)-1)
print(arr)


# Python program for implementation of MergeSort
# def mergeSort(arr):
#     if len(arr) > 1:
#         mid = len(arr) // 2  # Finding the mid of the array
#         L = arr[:mid]  # Dividing the array elements
#         R = arr[mid:]  # into 2 halves
#
#         mergeSort(L)  # Sorting the first half
#         mergeSort(R)  # Sorting the second half
#
#         i = j = k = 0
#
#         # Copy data to temp arrays L[] and R[]
#         while i < len(L) and j < len(R):
#             if L[i] < R[j]:
#                 arr[k] = L[i]
#                 i += 1
#             else:
#                 arr[k] = R[j]
#                 j += 1
#             k += 1
#
#         # Checking if any element was left
#         while i < len(L):
#             arr[k] = L[i]
#             i += 1
#             k += 1
#
#         while j < len(R):
#             arr[k] = R[j]
#             j += 1
#             k += 1
#
#
# # Code to print the list
# def printList(arr):
#     for i in range(len(arr)):
#         print(arr[i], end=" ")
#     print()
#
#
# # driver code to test the above code
# if __name__ == '__main__':
#     arr = [12, 11, 13, 5, 6, 7]
#     print("Given array is", end="\n")
#     printList(arr)
#     mergeSort(arr)
#     print("Sorted array is: ", end="\n")
#     printList(arr)

