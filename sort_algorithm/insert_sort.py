import time
import numpy as np
def insert_sort(listt):
    for i in range(len(listt)):
        current_val = listt[i]
        for j in range(i+1):
            if current_val < listt[j]:
                listt.insert(j, current_val)
                listt.pop(i+1)
                break

size = 1000
my_arr = list(np.random.randint(0, 500, size=size)) # 10M
start = time.time()
insert_sort(my_arr)
stop = time.time()
print("Bubble sort run {} elements in {}s".format(size, stop - start)) # 10000 elements run in 6s

