import time
import numpy as np

def bubble_sort(arr):
    for i in range(len(arr)):
        for j in range(i+1, len(arr)):
            if arr[i] > arr[j]:
                arr[i], arr[j] = arr[j], arr[i]
    return arr

size = 10000
my_arr = list(np.random.randint(0, 500, size=size)) # 10M
start = time.time()
bubble_sort(my_arr)
stop = time.time()
print("Bubble sort run {} elements in {}s".format(size, stop - start)) # 10000 elements run in 6s
# print(my_arr)