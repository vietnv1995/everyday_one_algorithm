def heapify(arr, n, i):
    largest = i
    l = 2*i + 1
    r = 2*i + 2
    if l < n and arr[l] > arr[largest]:
        largest = l
    if r < n and arr[r] > arr[largest]:
        largest = r
    if largest != i:
        arr[i], arr[largest] = arr[largest], arr[i]
        heapify(arr, n, largest)

def heap_sort(arr, n):
    j = int(n/2 - 1)
    for i in range(0, j+1):
        heapify(arr, n, j)
        j = j-1

    k = n-1
    for i in range(0, k+1):
        arr[0], arr[k] = arr[k], arr[0]
        heapify(arr, k, 0)
        k = k-1

arr = [12, 11, 13, 5, 6, 7, -5]
heap_sort(arr, len(arr))
print(arr)
import time
a =time.time()