import time
import numpy as np
import sys
sys.setrecursionlimit(1500)

def recursive_insert_sort(arr, n):
    if n == 1:
        return arr
    else:
        recursive_insert_sort(arr, n-1)
        try:
            last = arr[n-1]
            for i in range(n-1):
                if last<arr[i]:
                    arr.insert(i, last)
                    arr.pop(n)
                    break

        except:
            pass

size = 1000
my_arr = list(np.random.randint(0, 500, size=size)) # 10M
start = time.time()
recursive_insert_sort(my_arr, len(my_arr))
stop = time.time()
print("Bubble sort run {} elements in {}s".format(size, stop - start)) # 10000 elements run in 6s